
import time

from collections import OrderedDict

import numpy as np
import os

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1') 
import theano
import theano.tensor as T

import lasagne
from lasagne.nonlinearities import ScaledTanH

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from theano.scalar.basic import UnaryScalarOp, same_out_nocomplex
from theano.tensor.elemwise import Elemwise

# Our own rounding function, that does not set the gradient to 0 like Theano's
class Round3(UnaryScalarOp):
    
    def c_code(self, node, name, (x,), (z,), sub):
        return "%(z)s = round(%(x)s);" % locals()
    
    def grad(self, inputs, gout):
        (gz,) = gout
        return gz, 
        
round3_scalar = Round3(same_out_nocomplex, name='round3')
round3 = Elemwise(round3_scalar)

def hard_sigmoid(x):
    return T.clip((x+1.)/2.,0,1)
    

# The neurons' activations binarization function
# It behaves like the sign function during forward propagation
# And like:
#   hard_tanh(x) = 2*hard_sigmoid(x)-1 
# during back propagation

def binary_tanh_unit(x):
    return 2.*round3(hard_sigmoid(x))-1.
    
def ternary_tanh_unit(x):
    return round3(T.clip(x, -1, 1))

def binary_activation(x):
    return round3(T.clip(x/2., 0, 1))
    
def binary_sigmoid_unit(x):
    return round3(hard_sigmoid(x))
def sigmoid_relu(x):
    return T.clip(x, 0, 4)
def hard_fire(x):
    return round3(T.clip(x/2., 0, 1))
def hard_fire2(x):
    return round3(T.clip(x, 0, 1))    
def ternary_relu(x):
    return round3(T.clip(x, 0, 2))
def ternary_relu2(x):
    return round3(T.clip(x/2, 0, 2))
def relu_2bit(x):
    return round3(T.clip(x, 0, 3))
def relu_3bit(x):
    return round3(T.clip(x, 0, 7))
def relu_3bit_p5(x):
    return round3(T.clip(x*2, 0, 7))/2.
def relu_4bit(x):
    return round3(T.clip(x, 0, 15))
def relu_8bit(x):
    return round3(T.clip(x, 0, 255))
# relu 9 bit which will be fed to cosine similarity block as an input
def relu_9bit_cosine(x):
    # As this activation output will go to cosine similarity module, the value can only be between ~[-1.8, 1,8]. This
    # can be assumed to be betwenn [-2 to 2], as cosine similarity block expects the value in this range. 
    # To apply relu in this range 1 bit is used for for sign even, we are left with 8 bits, of this to represent 
    # [0. 1.8] we can use 1 integer bit and 7 fraction bits. With relu, 1 bit integer and 7 bits of fraction,
    # the range is now [0, 2-2**-7]. This is the range on which we will clip input. To apply round3 quantization
    # function, first convert this number of integer by multipying 2**7. Then apply round3. After quantization
    # again multiply by 2**-7 to bring in original range. 
    return round3(T.clip(x, 0, 2-2**-7) * 2**7) * 2.**-7 

def relu_12bit_cosine(x):
    # 4 bit integer including sign bit, 8 bit fraction
    return round3(T.clip(x, 0, 8-2**-8) * 2**8) * 2.**-8

def relu_16bit_cosine(x):
    # 8 bit integer 8 bit fraction
    return round3(T.clip(x, 0, 2^7-2^-8) * 2**8) * 2**-8

# scaled tanh 9 bit which will be fed to cosine similarity block as an input
def stanh_9bit_cosine(x):
    # for scaled tanh activation scale sout us 1.7159*.8. This .8 is something to do how scaled tanh is generated in 
    # matlab, whigh should be compared with this version. First scaled tanh is applied then the output is quantized
    # to 9 bits. 1 bit is for sign, 1 integer bit and 7 fraction bits. Output of scaled tanh is first multiplied with
    # 2**7 to left shift scaledtanh output and remove all the fraction bits, so that rounding operation can be 
    # applied on this number. Now after quantization again right shift to get back 7 fraction bits.
    return round3((lasagne.nonlinearities.tanh(x*2./3) * 1.7159*.8) * 2**7) * 2**-7
    #return round3(ScaledTanH(scale_in=2./3, scale_out=1.7159*.8) * 2**7) * 2**-7

# The weights' binarization function, 
# taken directly from the BinaryConnect github repository 
# (which was made available by his authors)
def binarization(W,H,binary=True,deterministic=False,stochastic=False,srng=None):
    
    # (deterministic == True) <-> test-time <-> inference-time
    if not binary or (deterministic and stochastic):
        # print("not binary")
        Wb = W
    
    else:
        
        # [-1,1] -> [0,1]
        Wb = hard_sigmoid(W/H)
        # Wb = T.clip(W/H,-1,1)
        
        # Stochastic BinaryConnect
        if stochastic:
        
            # print("stoch")
            Wb = T.cast(srng.binomial(n=1, p=Wb, size=T.shape(Wb)), theano.config.floatX)

        # Deterministic BinaryConnect (round to nearest)
        else:
            # print("det")
            Wb = T.round(Wb)
        
        # 0 or 1 -> -1 or 1
        Wb = T.cast(T.switch(Wb,H,-H), theano.config.floatX)
    
    return Wb

def quaternarization(W,H,quaternary=True,deterministic=False,stochastic=False,quant_style="uniform", srng=None):
    # stochastic quaternarization not implemented yet
    if not quaternary or (deterministic and stochastic):
        Wq = W
    else:
        if quant_style == "uniform":
            print("uniform qunatization to (-1, -1/3, 1/3, 1)")
            Wq = T.cast(T.switch(T.gt(W, H*2./3.), H, T.switch(T.gt(W, 0.), H/3., T.switch(T.gt(W, -2.*H/3.), -H/3., -H))), theano.config.floatX)
        else:
            print("shift_bits qunatization to (-1, -1/2, 1/2, 1)")
            Wq = T.cast(T.switch(T.gt(W, H*3./4.), H, T.switch(T.gt(W, 0.), H/2., T.switch(T.gt(W, -3.*H/4.), -H/2., -H))), theano.config.floatX)
    return Wq

def octonarization(W,H,octonary=True,deterministic=False,stochastic=False,quant_style="uniform", srng=None):
    # stochastic octonarization not implemented yet
    if not octonary or (deterministic and stochastic):
        Wo = W
    else:
        if quant_style == "uniform":
            Wo = T.cast(T.switch(T.gt(W, H*6./7.), H, T.switch(T.gt(W, H*4./7.), H*5./7., \
                    T.switch(T.gt(W, H*2./7.), H*3./7., T.switch(T.gt(W, 0.), H/7., T.switch(T.gt(W, -H*2./7.), -H/7.,\
                    T.switch(T.gt(W, -H*4./7.), -H*3./7., T.switch(T.gt(W, -H*6./7.), -H*5./7., -H))))))), theano.config.floatX)
        else:
            Wo = T.cast(T.switch(T.gt(W, H*3./4.), H, T.switch(T.gt(W, H*3./8.), H/2., T.switch(T.gt(W, H*3./16.), H/4.,\
                    T.switch(T.gt(W, 0.), H/8., T.switch(T.gt(W, -H*3./16.), -H/8., T.switch(T.gt(W, -H*3./8.), -H/4.,\
                    T.switch(T.gt(W, -H*3./4.), -H/2., -H))))))), theano.config.floatX)
    return Wo    

def four_bit_qunatization(W,H,sixteenary=True,deterministic=False,stochastic=False,quant_style="uniform", srng=None):
    if not sixteenary or (deterministic and stochastic):
        Wh = W
    else:
        if quant_style == "uniform":
            Wh = T.cast(T.switch(T.gt(W, H*14./15.), H, T.switch(T.gt(W, H*12./15.), H*13./15., \
                    T.switch(T.gt(W, H*10./15.), H*11./15., T.switch(T.gt(W, H*8./15.), H*9./15., \
                    T.switch(T.gt(W, H*6./15.), H*7./15., T.switch(T.gt(W, H*4./15.), H*5./15., \
                    T.switch(T.gt(W, H*2./15.), H*3./15., T.switch(T.gt(W, 0.), H/15., \
                        T.switch(T.gt(W, -H*2./15.), -H/15., T.switch(T.gt(W, -H*4./15.), -H*3./15., \
                                T.switch(T.gt(W, -H*6./15.), -H*5./15., T.switch(T.gt(W, -H*8./15.), -H*7./15., \
                                T.switch(T.gt(W, -H*10./15.), -H*9./15., T.switch(T.gt(W, -H*12./15.), -H*11./15., \
                                T.switch(T.gt(W, -H*14./15.), -H*13./15., -H))))))))))))))), dtype=theano.config.floatX)
        else:
            Wh = T.cast(T.switch(T.gt(W, H*3./4.), H, T.switch(T.gt(W, H*3./8.), H/2., T.switch(T.gt(W, H*3./16.), H/4.,\
                    T.switch(T.gt(W, H*3./32.), H/8., T.switch(T.gt(W, H*3./64.), H/16., \
                    T.switch(T.gt(W, H*3./128.), H/32., T.switch(T.gt(W, H*3./256.), H/64., \
                    T.switch(T.gt(W, 0.), H/128., T.switch(T.gt(W, -H*3./256.), -H/128., \
                    T.switch(T.gt(W, -H*3./128.), -H/64., T.switch(T.gt(W, -H*3./64.), -H/32., \
                    T.switch(T.gt(W, -H*3./32.), -H/16., T.switch(T.gt(W, -H*3./16.), -H/8., \
                    T.switch(T.gt(W, -H*3./8.), -H/4., T.switch(T.gt(W, -H*3./4.), -H/2., -H))))))))))))))), dtype=theano.config.floatX)
    return Wh

def six_bit_quantization(W, H, sixtyfour=True, deterministic=False, stochastic=False, quant_style='uniform', srng=None):
    if not sixtyfour or (deterministic and stochastic):
        Wh = W
    else:
        target_bits = 6
        if quant_style == 'uniform':
            base = (2.**target_bits)-1.
            Wh = H * ((2./base) * T.round((base/2.)*(W/H + 1./base)) - 1./base)
        else:
            raise ValueError('quantization style not implemented')
    return Wh

def eight_bit_quantization(W,H,twofiftysix=True,deterministic=False,stochastic=False,quant_style="symm",srng=None):
    if not twofiftysix or (deterministic and stochastic):
        Wh = W
    else:
        # uniform is same as symm_eq
        if quant_style == 'uniform':
            target_bits = 8
            base = (2.**target_bits)-1.
            # below formula is for symm_eq quantization where for 2 bits quantization levels are -1, -.33, .33, 1
            # if instead of -1 to 1 the clipping range is -H to H then we have to scale x and y by H. This scaling is 
            # done on function obtained by performing normal uniform quantization
            Wh = H *((2./base) * T.round((base/2.)*(W/H + 1./base)) - 1./base)

        elif quant_style == "symm":
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            target_bits = 8
            base = 2.**(target_bits-1.)
            Wh = T.switch(T.ge(W*base,0.),T.ceil(W*base)/base,T.floor(W*base)/base)
            Wh = T.switch(T.eq(Wh,0.),1./base, Wh )
        else:
            raise ValueError('not correct quantization type')
    return Wh

def clip_quant_input(x):
    # input dataset has 1 sign bit, 2 fraction bits and 4 integer bits. To perform this quantization first clip input data in the 
    # range of [-8, 8-.25]. As min value can be 1000.00, which is -8 and min value and max is 0111.11 which is 8-.25.
    # Before performing round3 need to remove fractional part by mutiplying by 2**2 and then after rounding divide by
    # 2**-2
    return np.around(np.clip(x, -8, 8-(2**-2)) * 2**2) * 2**-2

# This class extends the Lasagne DenseLayer to support BinaryConnect
class DenseLayer(lasagne.layers.DenseLayer):
    
    def __init__(self, incoming, num_units, 
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", n_bits=1, quant_style="uniform", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.H = np.float32(np.sqrt(1.5/ (num_inputs + num_units)))
            # print("H = "+str(self.H))
            
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5/ (num_inputs + num_units)))

        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
        
        if self.binary:
            super(DenseLayer, self).__init__(incoming, num_units, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
            
        else:
            super(DenseLayer, self).__init__(incoming, num_units, **kwargs)
        
    def get_output_for(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        self.W = self.Wb
            
        rvalue = super(DenseLayer, self).get_output_for(input, **kwargs)
        
        self.W = Wr
        
        return rvalue

class DenseLayer_HCGS(lasagne.layers.DenseLayer):
    
    def __init__(self, incoming, num_units, hcgs_blk_size1, hcgs_percentage1, hcgs_blk_size2, hcgs_percentage2, equal_fanin=True,
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", equal_both=False, n_bits=1, quant_style="uniform", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.H = np.float32(np.sqrt(1.5/ (num_inputs + num_units)))
            # print("H = "+str(self.H))
            
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5/ (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5/ ((num_inputs + num_units)*hcgs_percentage1*hcgs_percentage2)))

        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
        
        if self.binary:
            super(DenseLayer_HCGS, self).__init__(incoming, num_units, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
            
        else:
            super(DenseLayer_HCGS, self).__init__(incoming, num_units, **kwargs)
        
        self.hcgs_percentage1 = hcgs_percentage1
        self.hcgs_blk_size1 = hcgs_blk_size1
        self.hcgs_percentage2 = hcgs_percentage2
        # the drop percentage will be used in second phase of hcgs where smaller blocks will be dropped within bigger one
        self.hcgs_drop2 = 1. - self.hcgs_percentage2
        self.hcgs_blk_size2 = hcgs_blk_size2

        # the last conv layer will be of shape (None,512,4,4). So the input size fo the dense layer is 512*4*4. The
        # shape 4,4 is due to 3 max pooling layers from 32,32.
        num_inputs = int(np.prod(incoming.output_shape[1:]))
        # assume num_inputs is an integer multiple of block size, similarily for the num_units
        num_blk_rows = int(np.ceil(num_inputs / self.hcgs_blk_size1))        
        num_blk_cols = int(np.ceil(num_units / self.hcgs_blk_size1))    

        # number of smaller blocks along rows and columns
        num_sblk_rows = int(np.ceil(self.hcgs_blk_size1/self.hcgs_blk_size2))
        num_sblk_cols = int(np.ceil(self.hcgs_blk_size1/self.hcgs_blk_size2))

        if self.hcgs_percentage1 == 1.0 and self.hcgs_percentage2 == 1.:
            W_mask = np.ones((num_inputs, num_units), dtype=theano.config.floatX)
        else:
            W_mask = np.zeros((num_inputs, num_units), dtype=theano.config.floatX)
            if equal_both == True:
                raise ValueError('equal both not implemented for hcgs')
               # sel_matrix = np.zeros((num_blk_rows, num_blk_cols), dtype='int8')
               # num_blk_cols_sel = int(num_blk_cols * self.hcgs_percentage1)
               # # Initial a regularized sel_matrix
               # for i in range(num_blk_rows):
               #     col_idx_list = (np.arange(num_blk_cols_sel) + i) % num_blk_cols
               #     sel_matrix[i, col_idx_list] = 1
               # # randomly permute rows
               # sel_matrix_permuted = np.random.permutation(sel_matrix)
               # for i in range(num_blk_rows):
               #     for j in range(num_blk_cols):
               #         if sel_matrix_permuted[i, j] == 1:
               #             W_mask[i*self.cgs_blk_size:(i+1)*self.cgs_blk_size, j*self.cgs_blk_size:(j+1)*self.cgs_blk_size] = 1
            else:
                if equal_fanin == True:
                    raise ValueError('equal fanin not implemented for hcgs')
                   # num_blk_rows_sel = int(num_blk_rows * self.cgs_percentage)
                   # for i in range(num_blk_cols):
                   #     rand_choice = np.random.choice(num_blk_rows, size=(num_blk_rows_sel,), replace=False)
                   #     for j in range(num_blk_rows_sel):
                   #         start_row_index = rand_choice[j]*self.cgs_blk_size
                   #         stop_row_index = np.minimum((rand_choice[j]+1)*self.cgs_blk_size, num_inputs)
                   #         W_mask[start_row_index:stop_row_index, i*self.cgs_blk_size:(i+1)*self.cgs_blk_size] = 1
                else:
                    # here each row will have same number of blocks selected
                    num_blk_cols_sel = int(num_blk_cols * self.hcgs_percentage1)
                    # traverse through all rows and for each generate a list of column blocks to be non-sparse
                    for i in range(num_blk_rows):
                        rand_choice = np.random.choice(num_blk_cols, size=(num_blk_cols_sel,), replace=False)
                        for j in range(num_blk_cols_sel):
                            start_col_index = rand_choice[j]*self.hcgs_blk_size1
                            stop_col_index = np.minimum((rand_choice[j]+1)*self.hcgs_blk_size1, num_units)
                            W_mask[i*self.hcgs_blk_size1:(i+1)*self.hcgs_blk_size1, start_col_index:stop_col_index] = 1
                            
                            # number of smaller column blocks to be sparse. As the bigger block is all 1s or non-sparse
                            # need to change the vlaues for sparse blocks to 0
                            num_sblk_cols_sel = int(num_sblk_cols * self.hcgs_drop2)
                            # traverse for smaller blocks within a bigger block for each smaller row blocks 
                            for k in range(num_sblk_rows):
                                rand_schoice = np.random.choice(num_sblk_cols, size=(num_sblk_cols_sel,), replace=False)
                                # traverse for all columns to be dropped in a row for samller row blocks
                                for scol_index in rand_schoice:
                                    start_scol_index = start_col_index + scol_index * self.hcgs_blk_size2
                                    stop_scol_index = np.minimum(start_col_index + (scol_index+1) * self.hcgs_blk_size2,\
                                            num_inputs)
                                    W_mask[i*self.hcgs_blk_size1 + k*self.hcgs_blk_size2 : i*self.hcgs_blk_size1 + \
                                            (k+1) * self.hcgs_blk_size2, start_col_index + scol_index * self.hcgs_blk_size2:\
                                            start_col_index + (scol_index+1) * self.hcgs_blk_size2]=0

        #np.save('./data/hcgs_16x16_2x2dense.npy', W_mask)
        # self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        self.W_mask = W_mask
        if self.hcgs_percentage1 < 1.0 or self.hcgs_percentage2 < 1.:
            self.W = self.W_mask * self.W
    def get_output_for(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
            print("2 bits dense hcgs layer")
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)    
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.hcgs_percentage1 < 1.0 or self.hcgs_percentage2 < 1.:
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
        rvalue = super(DenseLayer_HCGS, self).get_output_for(input, **kwargs)
        
        self.W = Wr
        
        return rvalue

class DenseLayer_CGS(lasagne.layers.DenseLayer):
    
    def __init__(self, incoming, num_units, cgs_blk_size, cgs_percentage, equal_fanin=True,
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", equal_both=False, n_bits=1, quant_style="uniform", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.H = np.float32(np.sqrt(1.5/ (num_inputs + num_units)))
            # print("H = "+str(self.H))
            
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5/ (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5/ ((num_inputs + num_units)*cgs_percentage)))

        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
        
        if self.binary:
            super(DenseLayer_CGS, self).__init__(incoming, num_units, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
            
        else:
            super(DenseLayer_CGS, self).__init__(incoming, num_units, **kwargs)
        
        self.cgs_percentage = cgs_percentage
        self.cgs_blk_size = cgs_blk_size
        num_inputs = int(np.prod(incoming.output_shape[1:]))
        num_blk_rows = int(np.ceil(num_inputs / self.cgs_blk_size)) # assume num_inputs is an integer multiple of block size, similarily for the num_units
        num_blk_cols = int(np.ceil(num_units / self.cgs_blk_size)) 
        if self.cgs_percentage == 1.0:
            W_mask = np.ones((num_inputs, num_units), dtype=theano.config.floatX)
        else:
            W_mask = np.zeros((num_inputs, num_units), dtype=theano.config.floatX)
            if equal_both == True:
                sel_matrix = np.zeros((num_blk_rows, num_blk_cols), dtype='int8')
                num_blk_cols_sel = int(num_blk_cols * self.cgs_percentage)
                # Initial a regularized sel_matrix
                for i in range(num_blk_rows):
                    col_idx_list = (np.arange(num_blk_cols_sel) + i) % num_blk_cols
                    sel_matrix[i, col_idx_list] = 1
                # randomly permute rows
                sel_matrix_permuted = np.random.permutation(sel_matrix)
                for i in range(num_blk_rows):
                    for j in range(num_blk_cols):
                        if sel_matrix_permuted[i, j] == 1:
                            W_mask[i*self.cgs_blk_size:(i+1)*self.cgs_blk_size, j*self.cgs_blk_size:(j+1)*self.cgs_blk_size] = 1
            else:
                # this will select equal number of blocks in each column
                if equal_fanin == True:
                    num_blk_rows_sel = int(num_blk_rows * self.cgs_percentage)
                    for i in range(num_blk_cols):
                        rand_choice = np.random.choice(num_blk_rows, size=(num_blk_rows_sel,), replace=False)
                        for j in range(num_blk_rows_sel):
                            start_row_index = rand_choice[j]*self.cgs_blk_size
                            stop_row_index = np.minimum((rand_choice[j]+1)*self.cgs_blk_size, num_inputs)
                            W_mask[start_row_index:stop_row_index, i*self.cgs_blk_size:(i+1)*self.cgs_blk_size] = 1
                else:
                    # this option will enable equal number of selected blocks in each row
                    num_blk_cols_sel = int(num_blk_cols * self.cgs_percentage)
                    for i in range(num_blk_rows):
                        rand_choice = np.random.choice(num_blk_cols, size=(num_blk_cols_sel,), replace=False)
                        for j in range(num_blk_cols_sel):
                            start_col_index = rand_choice[j]*self.cgs_blk_size
                            stop_col_index = np.minimum((rand_choice[j]+1)*self.cgs_blk_size, num_units)
                            W_mask[i*self.cgs_blk_size:(i+1)*self.cgs_blk_size, start_col_index:stop_col_index] = 1
        #np.save('./data/cgs1_8x8dense.npy', W_mask)
        # self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        self.W_mask = W_mask
        if self.cgs_percentage < 1.0:
            self.W = self.W_mask * self.W
    def get_output_for(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
            print("2 bits dense cgs layer")
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)    
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==6:
            self.Wb = six_bit_quantization(self.W, self.H, self.binary, deterministic, self.stochastic,\
                    self.quant_style, self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.cgs_percentage < 1.0:
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
        rvalue = super(DenseLayer_CGS, self).get_output_for(input, **kwargs)
        
        self.W = Wr
        
        return rvalue
        
# This class extends the Lasagne Conv2DLayer to support BinaryConnect
class Conv2DLayer(lasagne.layers.Conv2DLayer):
    
    def __init__(self, incoming, num_filters, filter_size,
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", n_bits=1, quant_style="uniform", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))
        
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            # print("W_LR_scale = "+str(self.W_LR_scale))
            
        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
            
        if self.binary:
            super(Conv2DLayer, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)   
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer, self).__init__(incoming, num_filters, filter_size, **kwargs)    
        
    def convolve(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        self.W = self.Wb
            
        rvalue = super(Conv2DLayer, self).convolve(input, **kwargs)
        
        self.W = Wr
        
        return rvalue

class Conv2DLayer_shiftHCGS(lasagne.layers.Conv2DLayer):
    
    def __init__(self, incoming, num_filters, filter_size, hcgs_blkh_conv1, hcgs_blkw_conv1, hcgs_percentage1, 
            hcgs_blkh_conv2, hcgs_blkw_conv2, hcgs_percentage2, same_blk, n_bits=1, quant_style="uniform",
                 equal_fanin=False, equal_both=False, binary = True, stochastic = True, H=1.,W_LR_scale="Glorot",
                  **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))
        
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / ((num_inputs + num_units)*hcgs_percentage1*hcgs_percentage2)))
            # print("W_LR_scale = "+str(self.W_LR_scale))
            
        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
            
        if self.binary:
            super(Conv2DLayer_shiftHCGS, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)   
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer_shiftHCGS, self).__init__(incoming, num_filters, filter_size, **kwargs)    

        self.hcgs_percentage1 = hcgs_percentage1
        self.hcgs_blkh_conv1 = hcgs_blkh_conv1
        self.hcgs_blkw_conv1 = hcgs_blkw_conv1
        self.hcgs_percentage2 = hcgs_percentage2
        self.hcgs_drop2 = 1. - self.hcgs_percentage2
        self.hcgs_blkh_conv2 = hcgs_blkh_conv2
        self.hcgs_blkw_conv2 = hcgs_blkw_conv2

        num_inputs = int(incoming.output_shape[1])
        # number of blocks along the width/channel to not prune
        num_blk_inputs = int(np.ceil((num_inputs * filter_size[1]) / self.hcgs_blkw_conv1)) # assume num_inputs is an integer multiple of block size, similarily for the num_units
        # number of blocks along the filter/height to not prune
        num_blk_filters = int(np.ceil((num_filters * filter_size[0]) / self.hcgs_blkh_conv1))        
        
        # number of sub blocks on 2nd level of hcgs blocks along inputs/columns and filters/rows
        num_sblk_inputs = int(np.ceil(self.hcgs_blkw_conv1 / self.hcgs_blkw_conv2))
        num_sblk_filters =  int(np.ceil(self.hcgs_blkh_conv1 / self.hcgs_blkh_conv2))
        
        if self.hcgs_percentage1 == 1.0:
            W_mask = np.ones((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
        else:                
            
            # hcgs mask is initialized with same shape as filter matrix and allzero
            W_mask = np.zeros((num_filters * filter_size[0], num_inputs * filter_size[1]), dtype=theano.config.floatX)
            # equal both option will for each neuron have equal fanin and equal fanout.
            if equal_both == True:
                pass
            #     raise ValueError('hcgs not implemented for this case')
           #     sel_matrix = np.zeros((num_blk_filters, num_blk_inputs), dtype='int8')
           #     num_blk_inputs_sel = int(num_blk_inputs * self.hcgs_percentage1)
           #     # Initial a regularized sel_matrix
           #     for i in range(num_blk_filters):
           #         col_idx_list = (np.arange(num_blk_inputs_sel) + i) % num_blk_inputs
           #         sel_matrix[i, col_idx_list] = 1
           #     # randomly permute rows
           #     sel_matrix_permuted = np.random.permutation(sel_matrix)
           #     for i in range(num_blk_filters):
           #         for j in range(num_blk_inputs):
           #             if sel_matrix_permuted[i, j] == 1:
           #                 W_mask[i*self.hcgs_blkh_conv1:(i+1)*self.hcgs_blkh_conv1, j*self.hcgs_blkw_conv1:(j+1)*self.hcgs_blkw_conv1, :, :] = 1
            # equal fanin makes fanin for each neuron same. this means same number of channel blocks will be removed 
            # removed along a filter block. As each filter will have same number of channels after applying the mask,
            # when convolution is performed the hardware needed for each filter will be same as it applys on same number
            # of channels.
            else:
                if equal_fanin == True:
                    pass
                   # # number of blocks to pick along the channel/width/input
                   # raise ValueError('hcgs not implemented for this case')
                   # num_blk_inputs_sel = int(num_blk_inputs * self.hcgs_percentage1)
                   # for i in range(num_blk_filters):
                   #     rand_choice = np.random.choice(num_blk_inputs, size=(num_blk_inputs_sel,), replace=False)
                   #     for j in range(num_blk_inputs_sel):
                   #         start_input_index = rand_choice[j]*self.hcgs_blkw_conv1
                   #         stop_input_index = np.minimum((rand_choice[j]+1)*self.hcgs_blkw_conv1, num_inputs)
                   #         W_mask[i*self.hcgs_blkh_conv1:(i+1)*self.hcgs_blkh_conv1, start_input_index:stop_input_index, :, :] = 1
                # this block will generate mask list of the filter blocks which will be pruned for each channel block.  
                # Iterate over all the channel/input blocks and apply mask to all the blocks in filter block list. 
                # This ensured that same number of filter blocks are removed along a channel.
                else:
                    # find the number of filter blocks to not prune
                    num_blk_filters_sel = int(num_blk_filters * self.hcgs_percentage1)
                    # This loop will traverse through all the input or channel blocks
                    for i in range(num_blk_inputs):
                        # generate a list of filter blocks which will be non sparse. replace false makes sure that same
                        # block is not selected to be non sparse twice. Such list is generated for each input block
                        rand_choice = np.random.choice(num_blk_filters, size=(num_blk_filters_sel,), replace=False)
                        for j in range(num_blk_filters_sel):
                            start_filter_index = rand_choice[j]*self.hcgs_blkh_conv1
                            stop_filter_index = np.minimum((rand_choice[j]+1)*self.hcgs_blkh_conv1, num_filters)
                            # make elements of the mask as 1 based on hcgs percentage1. hcgs percent is the number of non
                            # sparse blocks. : in last two dimensions indicate that all the weights in the 2d filter 
                            # are selected and made non sparse
                            W_mask[start_filter_index:stop_filter_index, i*self.hcgs_blkw_conv1:(i+1)*self.hcgs_blkw_conv1] = 1
                            # selecting the filters to be dropped from a large cgs block. Here we use drop instead of 
                            # percentage, as we are going to make values 0 and not 1 unlike in the case loop.
                            # sblk stands for smaller blocks
                            num_sblk_filters_sel = int(num_sblk_filters * self.hcgs_drop2)
                            for k in range(num_sblk_inputs):
                                rand_schoice = np.random.choice(num_sblk_inputs, size=(num_sblk_filters_sel,), replace=False)
                                for sfilter_index in rand_schoice:
                                    start_sfilter_index = start_filter_index + sfilter_index * self.hcgs_blkh_conv2
                                    stop_sfilter_index = np.minimum(start_filter_index + (sfilter_index + 1) * \
                                            self.hcgs_blkh_conv2, num_filters)
                                    W_mask[start_sfilter_index : stop_sfilter_index, i*self.hcgs_blkw_conv1 + \
                                            k*self.hcgs_blkw_conv2 : i*self.hcgs_blkw_conv1 + (k+1)*self.hcgs_blkw_conv2,\
                                            ] = 0
            #np.save('./data/shifthcgs_mask_25x25_5x5.npy', W_mask)                            
            #same_blk =True
            # same blk takes exactly same set of 9 weights when the 2-d filters in a 3-d filter are arranged in a row
            if same_blk == True:
                W_mask = W_mask.reshape((num_filters, filter_size[0], num_inputs, filter_size[1]))
                W_mask = W_mask.transpose((0,2,1,3))
            else:
                # below, to generate a 3x3 2-d filter will take three 3x3 weights from the same row. This means if you
                # arrange 2-d filters of a 3-d filter in a row, the 9 weights seen for a filter will not be taken. 
                # Rather to generate 3x3 filter 3 weights will be taken from three different filters.
                W_mask = W_mask.reshape((num_filters, num_inputs, filter_size[0], filter_size[1]))     
        # self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        self.W_mask = W_mask
        if self.hcgs_percentage1 < 1.0:
            self.W = self.W_mask * self.W
    def convolve(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.hcgs_percentage1 < 1.0 :
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
            
        rvalue = super(Conv2DLayer_shiftHCGS, self).convolve(input, **kwargs)
        
        self.W = Wr
        
        return rvalue        



class Conv2DLayer_HCGS(lasagne.layers.Conv2DLayer):
    
    def __init__(self, incoming, num_filters, filter_size, hcgs_blkh_conv1, hcgs_blkw_conv1, hcgs_percentage1, 
            hcgs_blkh_conv2, hcgs_blkw_conv2, hcgs_percentage2, n_bits=1, quant_style="uniform",
                 equal_fanin=False, equal_both=False, binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))
        
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / ((num_inputs + num_units)*hcgs_percentage1*hcgs_percentage2)))
            # print("W_LR_scale = "+str(self.W_LR_scale))
            
        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
            
        if self.binary:
            super(Conv2DLayer_HCGS, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)   
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer_HCGS, self).__init__(incoming, num_filters, filter_size, **kwargs)    

        self.hcgs_percentage1 = hcgs_percentage1
        self.hcgs_blkh_conv1 = hcgs_blkh_conv1
        self.hcgs_blkw_conv1 = hcgs_blkw_conv1
        self.hcgs_percentage2 = hcgs_percentage2
        self.hcgs_drop2 = 1. - self.hcgs_percentage2
        self.hcgs_blkh_conv2 = hcgs_blkh_conv2
        self.hcgs_blkw_conv2 = hcgs_blkw_conv2
        num_inputs = int(incoming.output_shape[1])
        # number of blocks along the width/channel to not prune
        num_blk_inputs = int(np.ceil(num_inputs / self.hcgs_blkw_conv1)) # assume num_inputs is an integer multiple of block size, similarily for the num_units
        # number of blocks along the filter/height to not prune
        num_blk_filters = int(np.ceil(num_filters / self.hcgs_blkh_conv1))        
        
        # number of sub blocks on 2nd level of hcgs blocks along inputs/columns and filters/rows
        num_sblk_inputs = int(np.ceil(self.hcgs_blkw_conv1 / self.hcgs_blkw_conv2))
        num_sblk_filters =  int(np.ceil(self.hcgs_blkh_conv1 / self.hcgs_blkh_conv2))
        
        if self.hcgs_percentage1 == 1.0:
            W_mask = np.ones((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
        else:                
            # hcgs mask is initialized with same shape as filter matrix and allzero
            W_mask = np.zeros((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
            # equal both option will for each neuron have equal fanin and equal fanout.
            if equal_both == True:
                raise ValueError('hcgs not implemented for this case')
                sel_matrix = np.zeros((num_blk_filters, num_blk_inputs), dtype='int8')
                num_blk_inputs_sel = int(num_blk_inputs * self.hcgs_percentage1)
                # Initial a regularized sel_matrix
                for i in range(num_blk_filters):
                    col_idx_list = (np.arange(num_blk_inputs_sel) + i) % num_blk_inputs
                    sel_matrix[i, col_idx_list] = 1
                # randomly permute rows
                sel_matrix_permuted = np.random.permutation(sel_matrix)
                for i in range(num_blk_filters):
                    for j in range(num_blk_inputs):
                        if sel_matrix_permuted[i, j] == 1:
                            W_mask[i*self.hcgs_blkh_conv1:(i+1)*self.hcgs_blkh_conv1, j*self.hcgs_blkw_conv1:(j+1)*self.hcgs_blkw_conv1, :, :] = 1
            # equal fanin makes fanin for each neuron same. this means same number of channel blocks will be removed 
            # removed along a filter block. As each filter will have same number of channels after applying the mask,
            # when convolution is performed the hardware needed for each filter will be same as it applys on same number
            # of channels.
            else:
                if equal_fanin == True:
                    # number of blocks to pick along the channel/width/input
                    raise ValueError('hcgs not implemented for this case')
                    num_blk_inputs_sel = int(num_blk_inputs * self.hcgs_percentage1)
                    for i in range(num_blk_filters):
                        rand_choice = np.random.choice(num_blk_inputs, size=(num_blk_inputs_sel,), replace=False)
                        for j in range(num_blk_inputs_sel):
                            start_input_index = rand_choice[j]*self.hcgs_blkw_conv1
                            stop_input_index = np.minimum((rand_choice[j]+1)*self.hcgs_blkw_conv1, num_inputs)
                            W_mask[i*self.hcgs_blkh_conv1:(i+1)*self.hcgs_blkh_conv1, start_input_index:stop_input_index, :, :] = 1
                # this block will generate mask list of the filter blocks which will be pruned for each channel block.  
                # Iterate over all the channel/input blocks and apply mask to all the blocks in filter block list. 
                # This ensured that same number of filter blocks are removed along a channel.
                else:
                    # find the number of filter blocks to not prune
                    num_blk_filters_sel = int(num_blk_filters * self.hcgs_percentage1)
                    # This loop will traverse through all the input or channel blocks
                    for i in range(num_blk_inputs):
                        # generate a list of filter blocks which will be non sparse. replace false makes sure that same
                        # block is not selected to be non sparse twice. Such list is generated for each input block
                        rand_choice = np.random.choice(num_blk_filters, size=(num_blk_filters_sel,), replace=False)
                        for j in range(num_blk_filters_sel):
                            start_filter_index = rand_choice[j]*self.hcgs_blkh_conv1
                            stop_filter_index = np.minimum((rand_choice[j]+1)*self.hcgs_blkh_conv1, num_filters)
                            # make elements of the mask as 1 based on hcgs percentage1. hcgs percent is the number of non
                            # sparse blocks. : in last two dimensions indicate that all the weights in the 2d filter 
                            # are selected and made non sparse
                            W_mask[start_filter_index:stop_filter_index, i*self.hcgs_blkw_conv1:(i+1)*self.hcgs_blkw_conv1, :, :] = 1
                            # selecting the filters to be dropped from a large cgs block. Here we use drop instead of 
                            # percentage, as we are going to make values 0 and not 1 unlike in the case loop.
                            # sblk stands for smaller blocks
                            num_sblk_filters_sel = int(num_sblk_filters * self.hcgs_drop2)
                            for k in range(num_sblk_inputs):
                                rand_schoice = np.random.choice(num_sblk_inputs, size=(num_sblk_filters_sel,), replace=False)
                                for sfilter_index in rand_schoice:
                                    start_sfilter_index = start_filter_index + sfilter_index * self.hcgs_blkh_conv2
                                    stop_sfilter_index = np.minimum(start_filter_index + (sfilter_index + 1) * \
                                            self.hcgs_blkh_conv2, num_filters)
                                    W_mask[start_sfilter_index : stop_sfilter_index, i*self.hcgs_blkw_conv1 + \
                                            k*self.hcgs_blkw_conv2 : i*self.hcgs_blkw_conv1 
                                            + (k+1)*self.hcgs_blkw_conv2, :, :] = 0
        #np.save('./data/hcgs_mask_4x4_1x1.npy', W_mask)                            
        #self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        self.W_mask = W_mask
        if self.hcgs_percentage1 < 1.0:
            self.W = self.W_mask * self.W
    def convolve(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.hcgs_percentage1 < 1.0 :
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
            
        rvalue = super(Conv2DLayer_HCGS, self).convolve(input, **kwargs)
        
        self.W = Wr
        
        return rvalue        

# shift CGS is where the cgs blocks do not delete complete filter but some weight of it
class Conv2DLayer_shiftCGS(lasagne.layers.Conv2DLayer):
    
    def __init__(self, incoming, num_filters, filter_size, cgs_blkh_conv, cgs_blkw_conv, cgs_percentage, n_bits=1, quant_style="uniform",
                 equal_fanin=False, equal_both=False, binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", 
                 same_blk=True, withinfil=False, num_within=1, **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))
        
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / ((num_inputs + num_units)*cgs_percentage)))
            # print("W_LR_scale = "+str(self.W_LR_scale))
            
        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
            
        if self.binary:
            super(Conv2DLayer_shiftCGS, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)   
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer_shiftCGS, self).__init__(incoming, num_filters, filter_size, **kwargs)    

        self.cgs_percentage = cgs_percentage
        self.cgs_blkh_conv = cgs_blkh_conv
        self.cgs_blkw_conv = cgs_blkw_conv
        num_inputs = int(incoming.output_shape[1])
        # number of blocks along the width/channel to not prune
        num_blk_inputs = int(np.ceil((num_inputs*3) / self.cgs_blkw_conv)) # when filters are arranged 
        # number of blocks along the filter/height to not prune
        num_blk_filters = int(np.ceil((num_filters*3) / self.cgs_blkh_conv))        
        
        
        
        if self.cgs_percentage == 1.0:
            W_mask = np.ones((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
        else:                
            # cgs mask is initialized with same shape as filter matrix and allzero
            W_mask = np.zeros((num_filters * filter_size[0], num_inputs * filter_size[1]), dtype=theano.config.floatX)
            # equal both option will for each neuron have equal fanin and equal fanout.
            if equal_both == True:
               pass
               # sel_matrix = np.zeros((num_blk_filters, num_blk_inputs), dtype='int8')
               # num_blk_inputs_sel = int(num_blk_inputs * self.cgs_percentage)
               # # Initial a regularized sel_matrix
               # for i in range(num_blk_filters):
               #     col_idx_list = (np.arange(num_blk_inputs_sel) + i) % num_blk_inputs
               #     sel_matrix[i, col_idx_list] = 1
               # # randomly permute rows
               # sel_matrix_permuted = np.random.permutation(sel_matrix)
               # for i in range(num_blk_filters):
               #     for j in range(num_blk_inputs):
               #         if sel_matrix_permuted[i, j] == 1:
               #             W_mask[i*self.cgs_blkh_conv:(i+1)*self.cgs_blkh_conv, j*self.cgs_blkw_conv:(j+1)*self.cgs_blkw_conv, :, :] = 1
            ## below comments need to be updated
            # equal fanin makes fanin for each neuron same. this means same number of channel blocks will be removed 
            # removed along a filter block. As each filter will have same number of channels after applying the mask,
            # when convolution is performed the hardware needed for each filter will be same as it applys on same number
            # of channels.
            else:
                if equal_fanin == True:
                    pass
                   # num_blk_inputs_sel = int(num_blk_inputs * self.cgs_percentage)
                   # for i in range(num_blk_filters):
                   #     rand_choice = np.random.choice(num_blk_inputs, size=(num_blk_inputs_sel,), replace=False)
                   #     for j in range(num_blk_inputs_sel):
                   #         start_input_index = rand_choice[j]*self.cgs_blkw_conv
                   #         stop_input_index = np.minimum((rand_choice[j]+1)*self.cgs_blkw_conv, num_inputs)
                   #         W_mask[i*self.cgs_blkh_conv:(i+1)*self.cgs_blkh_conv, start_input_index:stop_input_index, :, :] = 1
                ## below comments need to be updated
                # this block will generate mask list of the filter blocks which will be pruned for each channel block.  
                # Iterate over all the channel/input blocks and apply mask to all the blocks in filter block list. 
                # This ensured that same number of filter blocks are removed along a channel.
                else:
                    # find the number of filter blocks to not prune
                    num_blk_filters_sel = int(num_blk_filters * self.cgs_percentage)
                    for i in range(num_blk_inputs):
                        # generate a list of filter blocks which will be non sparse. replace false makes sure that same
                        # block is not selected to be non sparse twice.
                        rand_choice = np.random.choice(num_blk_filters, size=(num_blk_filters_sel,), replace=False)
                        for j in range(num_blk_filters_sel):
                            start_filter_index = rand_choice[j]*self.cgs_blkh_conv
                            stop_filter_index = np.minimum((rand_choice[j]+1)*self.cgs_blkh_conv, num_filters * filter_size[0])
                            # make elements of the mask as 1 based on cgs percentage. cgs percent is the number of non
                            # sparse blocks. 
                            W_mask[start_filter_index:stop_filter_index, i*self.cgs_blkw_conv:(i+1)*self.cgs_blkw_conv] = 1
                    

        # option of withinfil means that it will remove some weights within filter. This is presently implemented for 
        # 1x1 cgs option. num_within is the number of weights to be removed from the filter
                            if withinfil==True:
                                sparse_w = np.random.choice(filter_size[0]*filter_size[1], size = num_within, replace=False)
                                for ss in sparse_w:
                                    sr = ss / filter_size[0]
                                    sc = ss % filter_size[0]
                                    #if i*self.cgs_blkw_conv + sc < num_filters *filter_size[0]:
                                    W_mask[start_filter_index+sr, i*self.cgs_blkw_conv + sc] = 0
                    
            #np.save('./data/shiftcgs_mask_1x1.npy', W_mask)
            # same blk takes exactly same set of 9 weights when the 2-d filters in a 3-d filter are arranged in a row
            if same_blk == True:
                W_mask = W_mask.reshape((num_filters, filter_size[0], num_inputs, filter_size[1]))
                W_mask = W_mask.transpose((0,2,1,3))
            else:
                # below, to generate a 3x3 2-d filter will take three 3x3 weights from the same row. This means if you
                # arrange 2-d filters of a 3-d filter in a row, the 9 weights seen for a filter will not be taken. 
                # Rather to generate 3x3 filter 3 weights will be taken from three different filters.
                W_mask = np.reshape(W_mask, (num_filters, num_inputs, filter_size[0], filter_size[1]))
        # self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        # below np.save was used to save the weight and then plot it pictorially in plot_mask.py file
        self.W_mask = W_mask
        if self.cgs_percentage < 1.0:
            self.W = self.W_mask * self.W
    def convolve(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.cgs_percentage < 1.0:
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
            
        rvalue = super(Conv2DLayer_shiftCGS, self).convolve(input, **kwargs)
        
        self.W = Wr
        
        return rvalue        

class Conv2DLayer_CGS(lasagne.layers.Conv2DLayer):
    
    def __init__(self, incoming, num_filters, filter_size, cgs_blkh_conv, cgs_blkw_conv, cgs_percentage, n_bits=1, quant_style="uniform",
                 equal_fanin=False, equal_both=False, binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", **kwargs):
        
        self.binary = binary
        self.stochastic = stochastic
        self.n_bits = n_bits
        self.quant_style = quant_style
        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))
        
        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            # self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / ((num_inputs + num_units)*cgs_percentage)))
            # print("W_LR_scale = "+str(self.W_LR_scale))
            
        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))
            
        if self.binary:
            super(Conv2DLayer_CGS, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)   
            # add the binary tag to weights            
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer_CGS, self).__init__(incoming, num_filters, filter_size, **kwargs)    

        self.cgs_percentage = cgs_percentage
        self.cgs_blkh_conv = cgs_blkh_conv
        self.cgs_blkw_conv = cgs_blkw_conv
        num_inputs = int(incoming.output_shape[1])
        # number of blocks along the width/channel to not prune
        num_blk_inputs = int(np.ceil(num_inputs / self.cgs_blkw_conv)) # assume num_inputs is an integer multiple of block size, similarily for the num_units
        # number of blocks along the filter/height to not prune
        num_blk_filters = int(np.ceil(num_filters / self.cgs_blkh_conv))        
        
        if self.cgs_percentage == 1.0:
            W_mask = np.ones((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
        else:                
            # cgs mask is initialized with same shape as filter matrix and allzero
            W_mask = np.zeros((num_filters, num_inputs, filter_size[0], filter_size[1]), dtype=theano.config.floatX)
            # equal both option will for each neuron have equal fanin and equal fanout.
            if equal_both == True:
                sel_matrix = np.zeros((num_blk_filters, num_blk_inputs), dtype='int8')
                num_blk_inputs_sel = int(num_blk_inputs * self.cgs_percentage)
                # Initial a regularized sel_matrix
                for i in range(num_blk_filters):
                    col_idx_list = (np.arange(num_blk_inputs_sel) + i) % num_blk_inputs
                    sel_matrix[i, col_idx_list] = 1
                # randomly permute rows
                sel_matrix_permuted = np.random.permutation(sel_matrix)
                for i in range(num_blk_filters):
                    for j in range(num_blk_inputs):
                        if sel_matrix_permuted[i, j] == 1:
                            W_mask[i*self.cgs_blkh_conv:(i+1)*self.cgs_blkh_conv, j*self.cgs_blkw_conv:(j+1)*self.cgs_blkw_conv, :, :] = 1
            # equal fanin makes fanin for each neuron same. this means same number of channel blocks will be removed 
            # removed along a filter block. As each filter will have same number of channels after applying the mask,
            # when convolution is performed the hardware needed for each filter will be same as it applys on same number
            # of channels.
            else:
                if equal_fanin == True:
                    num_blk_inputs_sel = int(num_blk_inputs * self.cgs_percentage)
                    for i in range(num_blk_filters):
                        rand_choice = np.random.choice(num_blk_inputs, size=(num_blk_inputs_sel,), replace=False)
                        for j in range(num_blk_inputs_sel):
                            start_input_index = rand_choice[j]*self.cgs_blkw_conv
                            stop_input_index = np.minimum((rand_choice[j]+1)*self.cgs_blkw_conv, num_inputs)
                            W_mask[i*self.cgs_blkh_conv:(i+1)*self.cgs_blkh_conv, start_input_index:stop_input_index, :, :] = 1
                # this block will generate mask list of the filter blocks which will be pruned for each channel block.  
                # Iterate over all the channel/input blocks and apply mask to all the blocks in filter block list. 
                # This ensured that same number of filter blocks are removed along a channel.
                else:
                    # find the number of filter blocks to not prune
                    num_blk_filters_sel = int(num_blk_filters * self.cgs_percentage)
                    for i in range(num_blk_inputs):
                        # generate a list of filter blocks which will be non sparse. replace false makes sure that same
                        # block is not selected to be non sparse twice.
                        rand_choice = np.random.choice(num_blk_filters, size=(num_blk_filters_sel,), replace=False)
                        for j in range(num_blk_filters_sel):
                            start_filter_index = rand_choice[j]*self.cgs_blkh_conv
                            stop_filter_index = np.minimum((rand_choice[j]+1)*self.cgs_blkh_conv, num_filters)
                            # make elements of the mask as 1 based on cgs percentage. cgs percent is the number of non
                            # sparse blocks. : in last two dimensions indicate that all the weights in the 2d filter 
                            # are selected and made non sparse
                            W_mask[start_filter_index:stop_filter_index, i*self.cgs_blkw_conv:(i+1)*self.cgs_blkw_conv, :, :] = 1
        # self.W_mask = self.add_param(W_mask, (num_inputs, num_units), name="W_mask", trainable=False)
        # below np.save was used to save the weight and then plot it pictorially in plot_mask.py file
        #np.save('./data/cgs_mask.npy', W_mask)
        self.W_mask = W_mask
        if self.cgs_percentage < 1.0:
            self.W = self.W_mask * self.W
    def convolve(self, input, deterministic=False, **kwargs):
        if self.n_bits == 1:
            self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        if self.n_bits == 2:
            self.Wb = quaternarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 3:
            self.Wb = octonarization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits == 4:
            self.Wb = four_bit_qunatization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,self._srng)
        if self.n_bits ==8:
            # use symm quantizaiton for 8 bits as creating T.switch condition for 256 values is difficult. 
            # symm quantization will do -1, -.5, .5, 1 for 2 bit -1 to 1 quantization
            self.Wb = eight_bit_quantization(self.W,self.H,self.binary,deterministic,self.stochastic,self.quant_style,\
                    self._srng)
        Wr = self.W
        if self.cgs_percentage < 1.0:
            self.W = self.Wb * self.W_mask
        else:
            self.W = self.Wb
            
        rvalue = super(Conv2DLayer_CGS, self).convolve(input, **kwargs)
        
        self.W = Wr
        
        return rvalue        
        
# This function computes the gradient of the binary weights
def compute_grads(loss,network):
        
    layers = lasagne.layers.get_all_layers(network)
    grads = []
    
    for layer in layers:
    
        params = layer.get_params(binary=True)
        if params:
            # print(params[0].name)
            grads.append(theano.grad(loss, wrt=layer.Wb))
                
    return grads

# This functions clips the weights after the parameter update
def clipping_scaling(updates,network):
    
    layers = lasagne.layers.get_all_layers(network)
    updates = OrderedDict(updates)
    
    for layer in layers:
    
        params = layer.get_params(binary=True)
        for param in params:
            print("W_LR_scale = "+str(layer.W_LR_scale))
            print("H = "+str(layer.H))
            updates[param] = param + layer.W_LR_scale*(updates[param] - param)
            updates[param] = T.clip(updates[param], -layer.H,layer.H)     

    return updates
def scaling(updates,network):
    layers = lasagne.layers.get_all_layers(network)
    updates = OrderedDict(updates)
    
    for layer in layers:
    
        params = layer.get_params(binary=True)
        for param in params:
            print("W_LR_scale = "+str(layer.W_LR_scale))
            print("H = "+str(layer.H))
            updates[param] = param + layer.W_LR_scale*(updates[param] - param)   

    return updates
    
# Given a dataset and a model, this function trains the model on the dataset for several epochs
# (There is no default trainer function in Lasagne yet)
def train( debug,
            arch_name, 
            train_fn,val_fn,
            model,
            batch_size,
            LR_start,LR_decay,
            num_epochs,
            X_train,y_train,
            X_val,y_val,
            #X_test,y_test,
            save_path=None,
            shuffle_parts=1):
    
    # A function which shuffles a dataset
    def shuffle(X,y):
        
        # print(len(X))
        
        chunk_size = len(X)/shuffle_parts
        shuffled_range = range(chunk_size)
        
        X_buffer = np.copy(X[0:chunk_size])
        y_buffer = np.copy(y[0:chunk_size])
        
        for k in range(shuffle_parts):
            
            np.random.shuffle(shuffled_range)

            for i in range(chunk_size):
                
                X_buffer[i] = X[k*chunk_size+shuffled_range[i]]
                y_buffer[i] = y[k*chunk_size+shuffled_range[i]]
            
            X[k*chunk_size:(k+1)*chunk_size] = X_buffer
            y[k*chunk_size:(k+1)*chunk_size] = y_buffer
        
        return X,y
        
        # shuffled_range = range(len(X))
        # np.random.shuffle(shuffled_range)
        
        # new_X = np.copy(X)
        # new_y = np.copy(y)
        
        # for i in range(len(X)):
            
            # new_X[i] = X[shuffled_range[i]]
            # new_y[i] = y[shuffled_range[i]]
            
        # return new_X,new_y
    
    # This function trains the model a full epoch (on the whole dataset)
    def train_epoch(X,y,LR,epoch):
        
        loss = 0.
        #batches = len(X)/batch_size
        batches = int(np.ceil(len(X)/float(batch_size)))
        for i in range(batches):
            train_IX = range(i*batch_size, min((i+1)*batch_size, len(X)))
            #train_IX = np.random.choice(data_range, len(data_range), replace=True)
            loss += train_fn(X[train_IX], y[train_IX], LR)
            #print 'accumulated batch loss', loss         
        # loss/=batches
        #loss = train_fn(X, y, LR)
        loss/=float(batches)
        return loss
    
    # This function tests the model a full epoch (on the whole dataset)
    def val_epoch(X,y):
        
        batches = int(np.ceil(len(X)/float(batch_size)))
        
        #for i in range(batches):
        #val_IX = np.random.choice(range(len(X)), len(X), replace=True)
        #val_loss = val_fn(X[val_IX], y[val_IX])
        val_loss = val_fn(X, y)
        #return val_loss/float(batches)
        #val_loss /= float(len(X))
        return val_loss
        #new_loss, new_err = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
            
       #     loss += new_loss
        
        #loss /= batches

        #return err, loss
    
    # shuffle the train set
    X_train,y_train = shuffle(X_train,y_train)
    best_val_loss = float('Inf')
    best_epoch = 1
    LR = LR_start
    
    # We iterate over epochs:
   
    try:
        os.makedirs('./{}/{}'.format(save_path, arch_name))
    except OSError:
        if not os.path.isdir('./{}/{}'.format(save_path, arch_name)):
            raise OSError('cant make director')
   
    train_loss_list = []
    val_loss_list = []
    epoch_start_time = time.time()
    for epoch in range(num_epochs):
        
        start_time = time.time()
        
        train_loss = train_epoch(X_train,y_train,LR,epoch)
        train_loss_list.append(train_loss)
        X_train,y_train = shuffle(X_train,y_train)
        
        val_loss = val_epoch(X_val,y_val)
        val_loss_list.append(val_loss)

        # test if validation error went down
        if val_loss < best_val_loss:
            
            best_val_loss = val_loss
            best_epoch = epoch+1
            
       #     test_err, test_loss = val_epoch(X_test,y_test)
            
        if save_path is not None and epoch==num_epochs-1:
            np.savez('{}/{}/{}'.format(save_path, arch_name, arch_name), *lasagne.layers.get_all_param_values(model))
        
        epoch_duration = time.time() - start_time
        
        # Then we print the results for this epoch:
        print('{}'.format(arch_name))
        print("Epoch "+str(epoch + 1)+" of "+str(num_epochs)+" took "+str(epoch_duration)+"s")
        print("  LR:                            "+str(LR))
        print("  training loss:                 "+str(train_loss))
        print("  test loss:                     "+str(val_loss))
        print("  best epoch:                    "+str(best_epoch))
        print("  best test loss:                "+str(best_val_loss))
        
        with open('./{}/{}/{}.txt'.format(save_path, arch_name,arch_name), 'a') as f:
            f.write('\n{}'.format(arch_name))
            f.write("\nEpoch "+str(epoch + 1)+" of "+str(num_epochs)+" took "+str(epoch_duration)+"s")
            f.write("\n  LR:                            "+str(LR))
            f.write("\n  training loss:                 "+str(train_loss))
            f.write("\n  test loss:                     "+str(val_loss))
            f.write("\n  best epoch:                    "+str(best_epoch))
            f.write("\n  best test loss:                "+str(best_val_loss))

        # decay the LR
        LR *= LR_decay
    
    from npz2mat import gen_mat
    gen_mat(arch_name)
    print('total time taken = {}mins'.format((time.time() - epoch_start_time)/60.))
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    plt.plot(train_loss_list, '-b', label='train_loss')
    plt.plot(val_loss_list, '-r', label='val_loss')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend(loc='upper right')
    plt.savefig('./data/{}/{}.png'.format(arch_name, arch_name))
    
    #plt.show()
    
        
#def fine_tuning_with_SA(
#        val_fn,
#        batch_size,
#        num_epochs,
#        model,
#        X_val,y_val,
#        X_test,y_test,
#        flip_ratio,
#        save_path=None
#        ):
#    def val_epoch(X,y):        
#        err = 0
#        loss = 0
#        batches = len(X)/batch_size
#        
#        for i in range(batches):
#            new_loss, new_err = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
#            err += new_err
#            loss += new_loss
#        
#        err = err / batches * 100
#        loss /= batches
#
#        return err, loss    
#    def flip_weights(W_old, fr):
#        W_new = []
#        for i in range(len(W_old)):
#            flip_mask = 1. - 2. * np.random.binomial(1, fr, size=W_old[i].shape).astype('float32')
#            W_new.append(W_old[i] * flip_mask)
#        return W_new
#    W = lasagne.layers.get_all_params(model, binary=True)
#    best_W = []
#    for i in range(len(W)):
#        best_W.append(W[i].get_value())
#    val_err, val_loss = val_epoch(X_val, y_val)
#    best_val_err = val_err
#    best_epoch = 0
#    for i in range(num_epochs):
#        start_time = time.time()
#        W_new = flip_weights(best_W, flip_ratio)
#        for j in range(len(W)):
#            W[j].set_value(W_new[j])
#        val_err, val_loss = val_epoch(X_val, y_val)
#        test_err, test_loss = val_epoch(X_test, y_test)
#        if val_err < best_val_err:
#            best_W = W_new
#            best_val_err = val_err
#            best_epoch = i + 1
#            if save_path is not None:
#                np.savez(save_path, *lasagne.layers.get_all_param_values(model))
#        epoch_duration = time.time() - start_time
#        
#        # Then we print the results for this epoch:
#        print("Epoch "+str(i + 1)+" of "+str(num_epochs)+" took "+str(epoch_duration)+"s")
#        print("  validation loss:               "+str(val_loss))
#        print("  validation error rate:         "+str(val_err)+"%")
#        print("  best epoch:                    "+str(best_epoch))
#        print("  best validation error rate:    "+str(best_val_err)+"%")
#        print("  test loss:                     "+str(test_loss))
#        print("  test error rate:               "+str(test_err)+"%") 
#
#def simplebn_test( arch_name, 
#            train_fn,val_fn,
#            model,
#            batch_size,
#            LR_start,LR_decay,
#            num_epochs,
#            X_train,y_train,
#            X_val,y_val,
#            X_test,y_test,
#            save_path=None,
#            shuffle_parts=1,
#            fliplr=False):
#    
#    
#    # This function tests the model a full epoch (on the whole dataset)
#    def val_epoch(X,y):
#        
#        err = 0
#        loss = 0
#        batches = len(X)/batch_size
#        
#        for i in range(batches):
#            new_loss, new_err = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
#            err += new_err
#            loss += new_loss
#        
#        err = err / batches * 100
#        loss /= batches
#
#        return err, loss
#    
#    with np.load('{}/{}/{}.npz'.format(save_path, arch_name, arch_name)) as fl:
#        param_values = [fl['arr_%d' % i] for i in range(len(fl.files))]
#    lasagne.layers.set_all_param_values(mlp, param_values)
#        
#    
#    # shuffle the train set
#    best_val_err = 100
#    best_epoch = 1
#    LR = LR_start
#    
#    # We iterate over epochs:
#   
#    try:
#        os.makedirs('./{}/{}'.format(save_path, arch_name))
#    except OSError:
#        if not os.path.isdir('./{}/{}'.format(save_path, arch_name)):
#            raise
#    
#        
#        start_time = time.time()
#        
#        X_train,y_train = shuffle(X_train,y_train)
#        
#        #val_err, val_loss = val_epoch(X_val,y_val)
#        test_err, test_loss = val_epoch(X_test,y_test)
#        
#        # test if validation error went down
#        
#        epoch_duration = time.time() - start_time
#        
#        # Then we print the results for this epoch:
#        print('{}'.format(arch_name))
#        #print("Epoch "+str(epoch + 1)+" of "+str(num_epochs)+" took "+str(epoch_duration)+"s")
#        #print("  LR:                            "+str(LR))
#        #print("  training loss:                 "+str(train_loss))
#        #print("  validation loss:               "+str(val_loss))
#        #print("  validation error rate:         "+str(val_err)+"%")
#        #print("  best epoch:                    "+str(best_epoch))
#        #print("  best validation error rate:    "+str(best_val_err)+"%")
#        print("  test loss:                     "+str(test_loss))
#        print("  test error rate:               "+str(test_err)+"%") 
#        
#        with open('./{}/{}/{}.txt'.format(save_path, arch_name,arch_name), 'a') as f:
#            f.write('\n{}'.format(arch_name))
#        #    f.write("\nEpoch "+str(epoch + 1)+" of "+str(num_epochs)+" took "+str(epoch_duration)+"s")
#        #    f.write("\n  LR:                            "+str(LR))
#        #    f.write("\n  training loss:                 "+str(train_loss))
#        #    f.write("\n  validation loss:               "+str(val_loss))
#        #    f.write("\n  validation error rate:         "+str(val_err)+"%")
#        #    f.write("\n  best epoch:                    "+str(best_epoch))
#        #    f.write("\n  best validation error rate:    "+str(best_val_err)+"%")
#            f.write("\n  test loss:                     "+str(test_loss))
#            f.write("\n  test error rate:               "+str(test_err)+"%") 
#
#        # decay the LR
#    return test_err
