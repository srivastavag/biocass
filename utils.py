import theano
import theano.tensor as T
import numpy as np

def cosine_similarity_loss(train_output, target, debug):
    if debug:
        out = np.array(range(718*256)).reshape((718,256)).astype('float32')
        tar = np.array(range(718)).astype('int32')
    delta = 1.e-8
    # keepdims keeps dimension as 1 over the axis where it is reduced so as to be able to perform broadcasting later
    normalized_output = train_output / T.sqrt(T.sum(T.sqr(train_output), axis=1, keepdims=True)).repeat(train_output.shape[1], axis=1)
    cosine_similarity_matrix = T.dot(normalized_output, normalized_output.transpose())

    # target01 has dimension of length 1 along axis=1. target 10 has axis=0 of length 1.
    # As target is ivector it is 1d tensor 
    target01 = target.dimshuffle([0, 'x'])
    target01 = target01.repeat(target.shape[0], axis=1)
    target10 = target.dimshuffle(['x', 0])
    target10 = target10.repeat(target.shape[0], axis=0)
    # dont know why *2 and -1 is used here. could have worked without it also
    diff_labels = T.clip(2*T.sqr(target10-target01)-1, 0, 1)
    diff_IX = T.nonzero(diff_labels)
    diff_cossim = cosine_similarity_matrix[diff_IX]
    dummy_tensor = T.zeros_like(target, dtype=theano.config.floatX)
    dummy_tensor = theano.ifelse.ifelse(T.eq(diff_cossim.size, 0), dummy_tensor, diff_cossim)
    diff_cossim_mean = T.switch(T.eq(diff_cossim.size, 0), 0, diff_cossim.mean())
    diff_cossim_std = T.switch(T.eq(diff_cossim.size, 0), 0, diff_cossim.std())
    # T.eye is to remove the diagonal elements from similarity computation
    same_labels = T.clip(2*T.sqr(target10-target01+T.eye(target.shape[0]))-1, -1, 0)
    same_IX = T.nonzero(same_labels)
    same_cossim = cosine_similarity_matrix[same_IX]
    #dummy_tensor = T.scalar('dummy', dtype=theano.config.floatX)
    #same_cossim = T.switch(T.eq(same_cossim.size, 0), dummy_tensor, same_cossim)
    same_cossim = theano.ifelse.ifelse(T.eq(same_cossim.size, 0), dummy_tensor, same_cossim)
    #same_cossim_mean = T.switch(T.eq(same_cossim.size, 0), 0, same_cossim.mean())
    #same_cossim_std = T.switch(T.eq(same_cossim.size, 0), 0, same_cossim.std())
    same_cossim_mean = same_cossim.mean()
    same_cossim_std = same_cossim.std()
    cossim_loss = (diff_cossim_mean - same_cossim_mean) / (diff_cossim_std + same_cossim_std + delta)
    return cossim_loss
