# Copyright 2017    Shihui Yin    Arizona State University

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

# Description: Convert npz model to matlab model for testing in matlab
# Created on 10/17/2017

from __future__ import print_function

import sys
#import h5py
import numpy as np
import scipy.io as sio


def gen_mat(arch_name):
    #model_suffix = ['5-40_R_160'] # '1-40_R_50', '1-40_Q_50', '5-50_R_30']
    for nn_ID in range(1):
        #data = np.load('./model/NN%d.npz' % (nn_ID+1))
        data = np.load('./data/{}/{}.npz'.format(arch_name, arch_name))
        W1 = data['arr_0']
        b1 = data['arr_1']
        W2 = data['arr_2']
        b2 = data['arr_3']
        input_dim = W1.shape[0]
        hid_dim = W1.shape[1]
        output_dim = W2.shape[1]
        # arranging weights and bias in the format needed by mat file. A row of W1b1 represents an output neuron's 
        # biases and then its weights coming from different input neurons.
        W1b1 = np.hstack((np.expand_dims(b1, axis=1), W1.transpose()))
        W2b2 = np.hstack((np.expand_dims(b2, axis=1), W2.transpose()))
        # using np.array when first dimension is same is not good idea. Instead create empty numpy array and then add 
        # members to it.
        #W_total = np.array([W1b1, W2b2, np.array([])], dtype=np.object)
        W_total = np.array([W1, np.expand_dims(b1, axis=0), W2, np.expand_dims(b2, axis=0)], dtype=np.object)
        nn = {"size": [input_dim, hid_dim, output_dim],
                "n": 3,
                "activation_function": "tanh_opt", # does this need to be changed
                "dropoutFraction": 0.1, # should this be changed as in matlab this is 0
                "testing": 1,
                "output": "softmax",
                "nonSparsityPenalty": 0,
                "W": W_total}
        sio.savemat('./data/{}/{}.mat'.format(arch_name, arch_name), {"nn":nn})
        
        
        
if __name__ == "__main__":
    arch_name = 'stanh_9bit_incosine_qi_qs_uniform_bi_nb2_bm1_8x8_40_bm2_8x8_50_nu256_dm10_bs2000'
    gen_mat(arch_name)    
