import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt

mask = np.load('./data/cgs1_8x8dense.npy')
#mask = np.load('./data/shiftcgs_mask_5x5sb.npy')
#mask = np.load('./data/shiftcgs_mask_6x6.npy')
#mask = np.load('./data/hcgs_16x16_2x2dense.npy')
#mask = np.load('./data/hcgs_mask_4x4_1x1.npy')
#mask = np.load('./data/cgs_mask.npy')
#immask = np.sum(mask, axis=(2,3))
#immask = mask[:100,:100]
#immask = mask[:1024,:1024]
xd = 256
yd = 256
xg = 8
yg = 8
immask = mask[:xd,:xd]
import pdb;pdb.set_trace()

# imshow drws th epicture, for final prinitng the picture use show()
#plt.imshow(immask, interpolation='nearest')
#

# took help from below forum to plot the grid
#https://stackoverflow.com/questions/38973868/adjusting-gridlines-and-ticks-in-matplotlib-imshow
#plt.figure(figsize=(1000, 1000))

plt.imshow(immask)
#plt.imshow(immask, interpolation='nearest')

ax = plt.gca()
# starting grid from 0 starts after the first element and -1 before the first element. Starting tixks at -.5 exactly
# start at starting of the figure
#ax.set_xticks(np.arange(-.5,128,1))
#ax.set_yticks(np.arange(-.5,128,1))
#ax.set_xticks(np.arange(-.5,100,2))
#ax.set_yticks(np.arange(-.5,100,2))
# Ticklables should start at 0, so starting labels at 0 instead of starting at -.5
#ax.set_xticklabels(np.arange(0,100,2))
#ax.set_yticklabels(np.arange(0,100,2))
#ax.set_xticklabels(np.arange(0,128,1))
#ax.set_yticklabels(np.arange(0,128,1))
# generating grid for major axis and both x and y axis.


ax.set_xticks(np.arange(-.5,xd,xg))
ax.set_yticks(np.arange(-.5,yd,yg))
ax.set_xticklabels(np.arange(0,xd,xg))
ax.set_yticklabels(np.arange(0,yd,yg))
# generating grid for major axis and both x and y axis.
ax.grid(which='major', axis='both')
plt.show()

