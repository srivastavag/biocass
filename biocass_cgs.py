# Copyright 2017    Shihui Yin    Arizona State University

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

# Description: VGG-like CNN for CIFAR-10 with optional CGS
# Created on 10/02/2017

from __future__ import print_function

import sys
import os
import time

import numpy as np
np.random.seed(1234) # for reproducibility?

import lasagne
import theano
import theano.tensor as T

import argparse
from ast import literal_eval as bool
import scipy.io as sio
import binary_net
import utils

#from pylearn2.datasets.zca_dataset import ZCA_Dataset   
#from pylearn2.datasets.cifar10 import CIFAR10 
#from pylearn2.utils import serial

from collections import OrderedDict



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="VGG-like CNN for CIFAR-10 with optional CGS")
    parser.add_argument('-ac', dest='activation', default='relu_9bit_cosine', help="activation function (tanh, \
            scaled_tanh, stanh_9bit_cosine, relu_9bit_cosine, relu, binary(+1/-1), \
            hard_fire(0,1), relu_2bit(0,1,2,3), ternary(0,1,2), relu_3bit(0,1,2,3,4,5,6,7), relu_4bit(0 to 15), \
            relu_8bit(0 to 255), relu_12bit_cosine, relu_16bit_cosine (default: %(default)s)")
    parser.add_argument('-bs', dest='batch_size', type=int, default=2000, help="batch size (default: %(default)d)")
    parser.add_argument('-al', dest='alpha', type=float, default=.1, help="alpha for batch normalization running mean (default: %(default).1f)")
    parser.add_argument('-ep', dest='epsilon', type=float, default=1e-4, help="epsilon for batch normalization (default: %(default).2e)")
    parser.add_argument('-cz', dest='center_zero', type=bool, default=True, help="normalize input to (-1,+1) if True, otherwise, to (0, 1) (default: %(default)s)")
    parser.add_argument('-pm1', dest='cgs_per_mlp1', type=float, default=1.0, help="cgs percentage for mlp layers 1, 1.0 means fully-connected and cgs is not applied (default: %(default).1f)")
    parser.add_argument('-bm1', dest='cgs_size_mlp1', type=int, default=8, help="cgs block size for mlp layers 1 (default: %(default)d)")
    parser.add_argument('-pm2', dest='cgs_per_mlp2', type=float, default=1.0, help="cgs percentage for mlp layers 2, 1.0 means fully-connected and cgs is not applied (default: %(default).1f)")
    parser.add_argument('-bm2', dest='cgs_size_mlp2', type=int, default=8, help="cgs block size for mlp layers 2 (default: %(default)d)")
    parser.add_argument('-ef1', dest='equal_fanin1', type=bool, default=True, help="equal fanin for cgs for layer 1 \
            (each neuron has equal fanin neurons) if True, equal fanout for cgs if False (default: %(default)s)")
    parser.add_argument('-eb1', dest='equal_both1', type=bool, default=False, help="for each neuron, it has equal fanin\
            and equal fanout neurons if True, will override equal_fanin option for layer 1 (default: %(default)s)")
    parser.add_argument('-ef2', dest='equal_fanin2', type=bool, default=False, help="equal fanin for cgs for layer 2\
            (each neuron has equal fanin neurons) if True, equal fanout for cgs if False (default: %(default)s)")
    parser.add_argument('-eb2', dest='equal_both2', type=bool, default=False, help="for each neuron, it has equal fanin\
            and equal fanout neurons if True, will override equal_fanin option for layer 2 (default: %(default)s)")
    parser.add_argument('-nu', dest='num_units', type=int, default=256, help="number of neurons in hidden fc layers (default: %(default)d)")
    parser.add_argument('-st', dest='stochastic', type=bool, default=False, help="stochastic rounding for binarization of weights if True (default: %(default)s)")
    parser.add_argument('-bi', dest='binary', type=bool, default=True, help="binarizing weights during feed-forward pass if True (default: %(default)s)")
    parser.add_argument('-ne', dest='num_epochs', type=int, default=200, help="number of epochs for training (default: %(default)d)")
    parser.add_argument('-ls', dest='LR_start', type=float, default=1e-3, help="start learning rate (default: %(default).1e)")
    parser.add_argument('-lf', dest='LR_fin', type=float, default=3e-7, help="finish learning rate (default: %(default).1e)")
    parser.add_argument('-sp', dest='save_path', default='cifar10_parameters.npz', help="save path for model parameters (default: %(default)s)")
    parser.add_argument('-lp', dest='load_path', default=None, help="path for pre-trained model parameters, if not None, the pre-trained model will be loaded before training starts (default: %(default)s)")
    parser.add_argument('-di', dest='dropout_input', type=float, default=0.0, help="dropout ratio input images (default: %(default).1f)")    
    parser.add_argument('-dm', dest='dropout_mlp', type=float, default=0.1, help="dropout ratio for mlp layers (default: %(default).1f)")    
    parser.add_argument('-lo', dest='loss', default='cosine_similarity', help="loss function (square_hinge, cubic_hinge, \
            or cross_entropy, cosine_similarity) (default: %(default)s)")
    parser.add_argument('-of', dest='output_function', default='linear', help="output function (linear, softmax) (default: %(default)s)")
    parser.add_argument('-cl', dest='clipping', type=bool, default=True, help="Clip the high-precision weights to [-1,1] if True (default: %(default)s)")
    parser.add_argument('-nb', dest='n_bits', type=int, default=2, help="weigh precision (default: %(default)s)")
    parser.add_argument('-qs', dest='quant_style', default="uniform", help="quantization style, options: uniform,\
            shift_bits, symm, (default: %(default)s)")
            #shift_bits, symm, set1_2bit, set2_2bit, set3_2bit (default: %(default)s)")
    parser.add_argument('-l2', dest='l2_reg', type=bool, default=False, help="l2 regularization (default: %(default)s)")
    parser.add_argument('-l2l', dest='l2_lambda', type=float, default=1e-5, help="l2 regularizaiton lambda\
            (default: %(default)s")
    parser.add_argument('-qi', dest='quantize_input', type=bool, default=True, help="As in hardware, input is \
            quantized to 1 bit sign, 3 bit integer and 2bit fraction")
    parser.add_argument('-H', dest= 'H', type=float, default=.75, help="when quantizing H is the clipping limit.\
            Usually this is 1, but for 2 bit case can be 3/4., 3/8., 3/16. corresponding quantization levels will \
            be [-3/4, -1/4, +1/4, +3/4], [-3/8, -1/8, +1/8, +3/8], and [-3/16, -1/16, +1/16, +3/16], (default: %(default)s)")
    args = parser.parse_args()
    print(args)
   
    debug = False
    # BN parameters
    batch_size = args.batch_size #50
    alpha = args.alpha #.1
    epsilon = args.epsilon #1e-4
    
    cgs_per_mlp1 = args.cgs_per_mlp1 #1.0
    cgs_size_mlp1 = args.cgs_size_mlp1 #16
    cgs_per_mlp2 = args.cgs_per_mlp2
    cgs_size_mlp2 = args.cgs_size_mlp2
    equal_fanin1 = args.equal_fanin1 #False
    equal_both1 = args.equal_both1 # False
    equal_fanin2 = args.equal_fanin2 #False
    equal_both2 = args.equal_both2 # False
    num_units = args.num_units #1024
    save_path = args.save_path
    W_mask_list=[]
    W_list=[]
    # BinaryOut
    if args.activation == 'relu_16bit_cosine':
        activation = binary_net.relu_16bit_cosine
        print("activation = binary_net.relu_16bit_cosine")
    if args.activation == 'relu_9bit_cosine':
        activation = binary_net.relu_9bit_cosine
        print("activation = binary_net.relu_9bit_cosine")
    if args.activation == 'relu_12bit_cosine':
        activation = binary_net.relu_12bit_cosine
        print("activation = binary_net.relu_12bit_cosine")
    if args.activation == 'stanh_9bit_cosine':
        activation = binary_net.stanh_9bit_cosine
        print("activation = binary_net.stanh_9bit_cosine")
    if args.activation == 'tanh':
        activation = lasagne.nonlinearities.tanh
        print("activation = lasagne.nonlinearities.tanh")
    if args.activation == 'scaled_tanh':
        activation = lasagne.nonlinearities.ScaledTanH(scale_in=2./3, scale_out=1.7159*.8) # alpha = 2./3 and beta = 1.7159
        print("activation = lasagne.nonlinearities.ScaledTanH")
    if args.activation == 'binary':
        activation = binary_net.binary_tanh_unit
        print("activation = binary_net.binary_tanh_unit")
    if args.activation == 'ternary':
        activation = binary_net.ternary_tanh_unit
        print("activation = binary_net.ternary_tanh_unit")
    if args.activation == 'relu' or args.activation == 'rectify':
        activation = lasagne.nonlinearities.rectify
        print("activation = lasagne.nonlinearities.rectify")
    if args.activation == 'hard_fire':
        activation = binary_net.hard_fire
        print("activation = binary_net.hard_fire")
    if args.activation == 'hard_fire2':
        activation = binary_net.hard_fire2
        print("activation = binary_net.hard_fire2")
    if args.activation == 'ternary_relu':
        activation = binary_net.ternary_relu
        print("activation = binary_net.ternary_relu")
    if args.activation == 'ternary_relu2':
        activation = binary_net.ternary_relu2
        print("activation = binary_net.ternary_relu2")    
    if args.activation == 'relu_2bit':
        activation = binary_net.relu_2bit
        print("activation = binary_net.relu_2bit")   
    if args.activation == 'relu_3bit':
        activation = binary_net.relu_3bit
        print("activation = binary_net.relu_3bit")  
    if args.activation == 'relu_4bit':
        activation = binary_net.relu_4bit
        print("activation = binary_net.relu_4bit")
    if args.activation == 'relu_8bit':
        activation = binary_net.relu_8bit
        print("activation = binary_net.relu_8bit")

    
   # if args.loss == 'square_hinge':
   #     print("loss = square hinge")
   # if args.loss == 'cubic_hinge':
   #     print("loss = cubic hinge")
   # if args.loss == 'neg_log_likelihood': # not implemented yet
   #     print("loss = negative log likelihood")
   # if args.loss == 'cross_entropy':
   #     print("loss = cross entropy")
    if args.loss == 'cosine_similarity':
        print("loss = cosine similarity")

    n_bits = args.n_bits
    quant_style = args.quant_style 
    # BinaryConnect    
    binary = args.binary #True
    stochastic = args.stochastic #False
    # H = "Glorot"
    #if quant_style=='set1_2bit': # [-3/4, -1/4, +1/4, +3/4] 
    #    H = 3/4.
    #elif quant_style=='set2_2bit': # [-3/8, -1/8, +1/8, +3/8]
    #    H = 3/8.
    #elif quant_style=='set3_2bit': # [-3/16, -1/16, +1/16, +3/16]
    #    H = 3/16.
    #else:
    #    H = 1.
    print("H = "+str(args.H))
    # W_LR_scale = 1.    
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    
    # Training parameters
    num_epochs = args.num_epochs #500
    # print("num_epochs = "+str(num_epochs))
    
    # Decaying LR 
    LR_start = args.LR_start #0.001
    # print("LR_start = "+str(LR_start))
    LR_fin = args.LR_fin #0.0000003
    # print("LR_fin = "+str(LR_fin))
    LR_decay = (LR_fin/LR_start)**(1./num_epochs)
    # print("LR_decay = "+str(LR_decay))
    # BTW, LR decay might good for the BN moving average...
    
     
    arch_name = '{}{}{}{}{}{}{}{}{}{}{}{}{}{}'.format('{}'.format(args.activation),\
            '_qi' if args.quantize_input==True else '', \
            '_qs_{}'.format(quant_style) if binary==True else '',\
            '_bi' if binary==True else '',\
            '_nb{}'.format(n_bits) if binary==True else '',\
            '_H{}'.format(int(args.H*1000)) if binary==True and args.H!=1 else '',\
            '_bm1_{}x{}_{}'.format(cgs_size_mlp1, cgs_size_mlp1, int(cgs_per_mlp1*100)) if cgs_per_mlp1<1. else '',\
            '_bm2_{}x{}_{}'.format(cgs_size_mlp2, cgs_size_mlp2, int(cgs_per_mlp2*100)) if cgs_per_mlp2<1. else '',\
            '_nu{}'.format(num_units),\
            '_di{}'.format(int(args.dropout_input*100)) if args.dropout_input>0. else '',\
            '_dm{}'.format(int(args.dropout_mlp*100)) if args.dropout_mlp>0. else '',\
            '_l2{:.0e}'.format(args.l2_lambda) if args.l2_reg==True and args.l2_lambda!=0 else '',\
            '_bs{}'.format(int(args.batch_size)), \
            '_dbg' if debug==True else ''
            )

    #valid_set = CIFAR10(which_set="train",start=train_set_size,stop = 50000)
    #test_set = CIFAR10(which_set="test")
    
    dataset_path = './ECG_SAIT_741/data'
    train_set_x = np.load('{}/trainX1norm.npy'.format(dataset_path))
    train_set_y = np.load('{}/trainY1norm.npy'.format(dataset_path))
    valid_set_x = np.load('{}/validX1norm.npy'.format(dataset_path))
    valid_set_y = np.load('{}/validY1norm.npy'.format(dataset_path))
    train_set_size = train_set_x.shape[0] 
    valid_set_size = valid_set_x.shape[0]
    # print("train_set_size = "+str(train_set_size))
    shuffle_parts = 1
    print("shuffle_parts = "+str(shuffle_parts))
    if args.quantize_input==True:
        train_set_x = binary_net.clip_quant_input(train_set_x)
        valid_set_x = binary_net.clip_quant_input(valid_set_x)
    train_set_label = train_set_y
    valid_set_label = valid_set_y
    

    print('Building the CNN...') 
    # Prepare Theano variables for inputs and targets
    input = T.matrix('inputs', dtype=theano.config.floatX)
    target = T.ivector('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    cnn = lasagne.layers.InputLayer(
            shape=(None, 160),
            input_var=input)
    # in lasagne cannot perform operations on layers. Need to create a new layer to add such operations. To perform 
    # mathematical operations need to use get_output function which will generate mathematical model of forward path
    # of all the layers and then we can perform mathematical operations on this. When CGS layer is define, get_output
    # function is defined for this CGS layer which will generate the forward path model when get_output is called for
    # last layer in the model.
    #if args.quantize_input==True:
        # input is quantize to 4 integer and 2 fraction bits
     #   cnn = binary_net.clip_input(cnn)
    
    if args.dropout_input > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_input)
    count_layers = 0

    cnn = binary_net.DenseLayer_CGS(
            cnn, 
            binary=binary,
            cgs_blk_size=cgs_size_mlp1,
            cgs_percentage=cgs_per_mlp1,
            equal_fanin=equal_fanin1,
            equal_both=equal_both1,
            nonlinearity=lasagne.nonlinearities.identity,
            W_LR_scale=W_LR_scale, stochastic=False,
            num_units=num_units,      
            H = args.H,
            n_bits=n_bits,
            quant_style=quant_style)      
    params = cnn.get_params()
    for param in params:
        if param.name == "W":
            W_list.append(param)
    W_mask_list.append(cnn.W_mask)
    
#    cnn = lasagne.layers.BatchNormLayer(
#            cnn,
#            epsilon=epsilon, 
#            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    if args.dropout_mlp > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_mlp)
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
                
    cnn = binary_net.DenseLayer_CGS(
            cnn, 
            binary=binary,
            cgs_blk_size=cgs_size_mlp2,
            cgs_percentage=cgs_per_mlp2,
            equal_fanin=equal_fanin2,
            equal_both=equal_both2,
            nonlinearity=lasagne.nonlinearities.identity,
            W_LR_scale=W_LR_scale, stochastic=False,
            num_units=num_units,      
            H = args.H,
            n_bits=n_bits,
            quant_style=quant_style)      
    params = cnn.get_params()
    for param in params:
        if param.name == "W":
            W_list.append(param)
    W_mask_list.append(cnn.W_mask)
    
#    cnn = lasagne.layers.BatchNormLayer(
#            cnn,
#            epsilon=epsilon, 
#            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))

    try:
        os.makedirs('./data/{}/'.format(arch_name))
    except:
        if not os.path.isdir('./data/{}/'.format(arch_name)):
            raise OSError('cant make directory')

    sio.savemat('./data/{}/cgs_mask.mat'.format(arch_name), {'w_mask_list': W_mask_list})
            
    train_output = lasagne.layers.get_output(cnn, deterministic=False)
    if debug:
        rr = train_output.eval({input:train_set_x[0:2000]})
    # squared hinge loss
   # if args.loss == 'square_hinge':
   #     loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))
   # if args.loss == 'cubic_hinge':
   #     loss = T.mean((T.maximum(0.,1.-target*train_output))**3)
   # if args.loss == 'cross_entropy':
   #     loss = T.mean(lasagne.objectives.categorical_crossentropy(train_output, target))
    if args.loss == 'cosine_similarity':
        loss = utils.cosine_similarity_loss(train_output, target, debug) 
    if args.l2_reg==True and args.l2_lambda!=0.:
        params = lasagne.layers.get_all_params(cnn, regularizable=True)
        l2_loss = 0.
        for param in params:
            l2_loss += T.sqr(param).sum()
        loss += args.l2_lambda * l2_loss

    if binary:
        
        # W updates
        W = lasagne.layers.get_all_params(cnn, binary=True)
        W_grads = binary_net.compute_grads(loss,cnn)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        if args.clipping == True:
            updates = binary_net.clipping_scaling(updates,cnn)
        else:
            updates = binary_net.scaling(updates,cnn)
        
        # other parameters updates
        params = lasagne.layers.get_all_params(cnn, trainable=True, binary=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())
        
    else:
        params = lasagne.layers.get_all_params(cnn, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)
    
    if cgs_per_mlp1 < 1.0 or cgs_per_mlp2 < 1.:
        for W, W_mask in zip(W_list, W_mask_list):
            updates[W] = updates[W] * W_mask
        
    test_output = lasagne.layers.get_output(cnn, deterministic=True)
    #import pdb; pdb.set_trace()
    
  #  if args.loss == 'square_hinge':
  #      test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
  #  if args.loss == 'cubic_hinge':
  #      test_loss = T.mean((T.maximum(0.,1.-target*test_output))**3)
  #  if args.loss == 'cross_entropy':
  #      test_loss = T.mean(lasagne.objectives.categorical_crossentropy(test_output, target))
    test_loss = utils.cosine_similarity_loss(test_output, target, loss)
    #test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX)
    
    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary) 
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], test_loss)

    if args.load_path is not None:
        print("Loading pretrained params...")
        with np.load(args.load_path) as f:
            W_values = [f['arr_%d' % i] for i in range(len(f.files))]
        params = lasagne.layers.get_all_params(cnn)
        for i in range(len(params)):
            params[i].set_value(W_values[i])
    
    print('Training...')
    binary_net.train(debug,
            arch_name,
            train_fn,val_fn,
            cnn,
            batch_size,
            LR_start,LR_decay,
            num_epochs,
            train_set_x,train_set_label,
            valid_set_x,valid_set_label,
            #test_set_x,test_set_y,
            save_path='./data',
            shuffle_parts=shuffle_parts)
