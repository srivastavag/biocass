import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


trainx = np.load('./ECG_SAIT_741/data/trainX1norm.npy')
plt.hist(trainx)
plt.savefig('./ECG_SAIT_741/data/trainx1hist.png')
plt.figure()
validx = np.load('./ECG_SAIT_741/data/validX1norm.npy')
import pdb;pdb.set_trace()
plt.hist(validx)
plt.savefig('./ECG_SAIT_741/data/validxhist.png')

        
